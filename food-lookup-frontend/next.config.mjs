const API_URL = process.env.NODE_ENV === 'development' ? process.env.DEV_API_URL : process.env.API_URL

/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'standalone',
    async rewrites() {
        return [{source: '/:slug*', destination: `${API_URL}/:slug*`}]
    },
};

export default nextConfig;
