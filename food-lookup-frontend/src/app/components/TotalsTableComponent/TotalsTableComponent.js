'use client'
import { Button, Flex, Table, Typography } from 'antd'

const TotalsTableComponent = ({ removeFromTotals, totalRows }) => {
    const columns = [
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'KCal',
            dataIndex: 'kcal',
            key: 'kcal',
        },
        {
            title: 'Protein',
            dataIndex: 'protein',
            key: 'protein',
        },
        {
            title: 'Fat',
            dataIndex: 'fat',
            key: 'fat',
        },
        {
            title: 'Carbs',
            dataIndex: 'carbs',
            key: 'carbs',
        },
        {
            title: 'Actions',
            render: (checked, row) => (
                <Flex gap={8}>
                    <Button onClick={() => removeFromTotals(row)}>
                        Remove from totals
                    </Button>
                </Flex>
            ),
        },
    ]

    return (
        <>
            <Typography.Title>Totals table</Typography.Title>
            <Table
                rowKey={(row) => row.id}
                columns={columns}
                dataSource={totalRows}
                summary={(pageData) => {
                    let totalKcal = 0
                    let totalProtein = 0
                    let totalCarbs = 0
                    let totalFats = 0

                    pageData.forEach(({ kcal, protein, carbs, fat }) => {
                        totalKcal += Number(kcal)
                        totalProtein += Number(protein)
                        totalCarbs += Number(carbs)
                        totalFats += Number(fat)
                    })

                    return (
                        <Table.Summary.Row>
                            <Table.Summary.Cell index={1} />
                            <Table.Summary.Cell index={2}>
                                <Typography.Text>{totalKcal}</Typography.Text>
                            </Table.Summary.Cell>
                            <Table.Summary.Cell index={3}>
                                <Typography.Text>
                                    {totalProtein}
                                </Typography.Text>
                            </Table.Summary.Cell>
                            <Table.Summary.Cell index={4}>
                                <Typography.Text>{totalCarbs}</Typography.Text>
                            </Table.Summary.Cell>
                            <Table.Summary.Cell index={5}>
                                <Typography.Text>{totalFats}</Typography.Text>
                            </Table.Summary.Cell>
                        </Table.Summary.Row>
                    )
                }}
            />
        </>
    )
}

export default TotalsTableComponent
