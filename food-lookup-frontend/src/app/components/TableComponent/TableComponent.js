'use client'
import { Button, Flex, Input, Table } from 'antd'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { SearchOutlined } from '@ant-design/icons'
import { debounce } from 'lodash'
import { useRouter } from 'next/navigation'

const TableComponent = ({ addToTotals, deleteRow, alreadyAdded }) => {
    const router = useRouter()
    const [ingredients, setIngredients] = useState([])
    const [pagination, setPagination] = useState({
        pageSize: 10,
        current: 1,
    })
    const [search, setSearch] = useState('')

    const getPaginatedItems = useCallback((pagination, search) => {
        fetch(
            `/ingredients?offset=${getPaginationOffset(pagination)}&limit=${pagination.pageSize}&description=${search}`
        ).then(async (res) => {
            const data = await res.json()
            setPagination((prevState) => ({
                ...prevState,
                total: data.total,
            }))
            setIngredients(data.page)
        })
    }, [])

    const onInputCallback = useCallback(
        (event) => {
            const newPagination = { ...pagination, current: 1 }
            setPagination(newPagination)
            getPaginatedItems(newPagination, event.target.value)
            setSearch(event.target.value)
        },
        [getPaginatedItems, pagination]
    )

    const debounceInput = useMemo(
        () => debounce((event) => onInputCallback(event), 600),
        [onInputCallback, pagination]
    )

    const getPaginationOffset = (pagination) => {
        if (pagination.current === 1 || pagination.current === 0) {
            return 0
        }
        if (pagination.current > 1) {
            return (Number(pagination?.current ?? 0) - 1) * pagination.pageSize
        }
    }

    useEffect(() => {
        getPaginatedItems(pagination, search)
    }, [])

    const columns = [
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'KCal',
            dataIndex: 'kcal',
            key: 'kcal',
        },
        {
            title: 'Protein',
            dataIndex: 'protein',
            key: 'protein',
        },
        {
            title: 'Fat',
            dataIndex: 'fat',
            key: 'fat',
        },
        {
            title: 'Carbs',
            dataIndex: 'carbs',
            key: 'carbs',
        },
        {
            title: 'Actions',
            render: (checked, row) => (
                <Flex gap={8}>
                    <Button
                        disabled={alreadyAdded(row)}
                        onClick={() => addToTotals(row)}
                    >
                        Add to totals
                    </Button>
                    <Button
                        onClick={() =>
                            deleteRow(row, () =>
                                getPaginatedItems(pagination, search)
                            )
                        }
                    >
                        Delete
                    </Button>
                    <Button
                        onClick={() => router.push(`/add-product?id=${row.id}`)}
                    >
                        Edit
                    </Button>
                </Flex>
            ),
        },
    ]

    return (
        <>
            <Flex style={{ gap: 20 }}>
                <Input
                    addonAfter={<SearchOutlined />}
                    onChange={debounceInput}
                    placeholder="Search for food"
                    allowClear
                    style={{ width: 200, marginRight: 'auto' }}
                />
                <Button onClick={() => router.push('/add-product')}>
                    Add Ingredient
                </Button>
            </Flex>

            <Table
                style={{ marginTop: 20 }}
                onChange={(pagination) => {
                    setPagination(pagination)
                    getPaginatedItems(pagination, search)
                }}
                pagination={pagination}
                rowKey={(row) => row.id}
                dataSource={ingredients}
                columns={columns}
            />
        </>
    )
}

export default TableComponent
