'use client'
import { Flex, Typography } from 'antd'
import TableComponent from '@/app/components/TableComponent/TableComponent'
import TotalsTableComponent from '@/app/components/TotalsTableComponent/TotalsTableComponent'
import { useState } from 'react'

export default function Home() {
    const [totalsRows, setTotalsRows] = useState([])

    const addItemToTotals = (row) => {
        setTotalsRows((prevState) => [...prevState, row])
    }

    const removeItemFromTotals = (row) => {
        setTotalsRows((prevState) =>
            prevState.filter((cRow) => cRow.id !== row.id)
        )
    }

    const deleteRow = async (row, refetch) => {
        removeItemFromTotals(row)
        fetch(`/delete/${row.id}`, {
            method: 'DELETE',
        }).then(() => refetch())
    }

    const alreadyAdded = (row) => totalsRows.some((cRow) => cRow.id === row.id)

    return (
        <Flex
            vertical
            align={'center'}
            style={{
                margin: 0,
                padding: 0,
                minHeight: '100vh',
                backgroundImage: 'url("images/5441.jpg")',
                backgroundSize: 'cover',
            }}
        >
            <Typography
                style={{
                    fontSize: '40px',
                    textTransform: 'uppercase',
                    color: '#faf9f9',
                }}
            >
                ingredients table
            </Typography>
            <div
                style={{
                    backgroundColor: 'white',
                    padding: 16,
                    borderRadius: 8,
                }}
            >
                <TableComponent
                    deleteRow={deleteRow}
                    alreadyAdded={alreadyAdded}
                    addToTotals={addItemToTotals}
                ></TableComponent>
                <TotalsTableComponent
                    removeFromTotals={removeItemFromTotals}
                    totalRows={totalsRows}
                ></TotalsTableComponent>
            </div>
        </Flex>
    )
}
