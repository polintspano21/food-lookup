'use client'
import {
    Button,
    Card,
    Flex,
    Form,
    Input,
    InputNumber,
    notification,
} from 'antd'
import { useRouter, useSearchParams } from 'next/navigation'
import { useEffect } from 'react'

const AddProduct = () => {
    const [api, contextHolder] = notification.useNotification()
    const openNotificationEdit = () => {
        api.success({
            message: `Successfully edited.`,
            description:
                'You have successfully edited this ingredient. Clicking this will take you to the tables page',
            placement: 'bottom',
            onClick: () => router.push('/'),
            style: { cursor: 'pointer' },
        })
    }

    const openNotificationAdd = () => {
        api.success({
            message: `Successfully added.`,
            description:
                'You have successfully added this ingredient. Clicking this will take you to the tables page',
            placement: 'bottom',
            onClick: () => router.push('/'),
        })
    }

    const router = useRouter()
    const params = useSearchParams()
    const [form] = Form.useForm()
    const shouldEdit = () => Boolean(params.get('id'))

    useEffect(() => {
        if (shouldEdit()) {
            fetch(`/ingredient/${params.get('id')}`).then(async (res) => {
                console.log(res)
                form.setFieldsValue(await res.json())
            })
        }
    }, [params])

    const onFinish = async (values) => {
        if (shouldEdit()) {
            await fetch(`/update/${params.get('id')}`, {
                headers: {
                    'content-type': 'application/json',
                },
                method: 'PUT',
                body: JSON.stringify(values),
            })
            openNotificationEdit()
        } else {
            await fetch('/add', {
                headers: {
                    'content-type': 'application/json',
                },
                method: 'POST',
                body: JSON.stringify(values),
            })
            openNotificationAdd()
        }
    }

    return (
        <>
            {contextHolder}
            <Flex
                style={{
                    margin: 0,
                    padding: 0,
                    minHeight: '100vh',
                    backgroundImage: 'url("images/5441.jpg")',
                    backgroundSize: 'cover',
                }}
                justify={'center'}
            >
                <Card style={{ width: 600, height: '100%', marginTop: 16 }}>
                    <Form
                        form={form}
                        name="basic"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        style={{ maxWidth: 600 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Description"
                            name="description"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your description!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="KCal"
                            name="kcal"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your kcal!',
                                },
                            ]}
                        >
                            <InputNumber />
                        </Form.Item>
                        <Form.Item
                            label="Protein"
                            name="protein"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your protein!',
                                },
                            ]}
                        >
                            <InputNumber />
                        </Form.Item>
                        <Form.Item
                            label="Fats"
                            name="fat"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your fats!',
                                },
                            ]}
                        >
                            <InputNumber />
                        </Form.Item>
                        <Form.Item
                            label="Carbs"
                            name="carbs"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your carbs!',
                                },
                            ]}
                        >
                            <InputNumber />
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Flex gap={8}>
                                <Button type="primary" htmlType="submit">
                                    {shouldEdit() ? 'Edit' : 'Add'}
                                </Button>
                                <Button
                                    onClick={() => router.push('/')}
                                    type="primary"
                                    htmlType="button"
                                >
                                    Go back
                                </Button>
                            </Flex>
                        </Form.Item>
                    </Form>
                </Card>
            </Flex>
        </>
    )
}

export default AddProduct
