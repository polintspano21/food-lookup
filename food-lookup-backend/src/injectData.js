import { createDatabase, getModels } from './db.js'
import { faker } from '@faker-js/faker'
import Ingredient from './models/Ingredient.js'

const vegetables = [
    "carrot", "broccoli", "tomato", "spinach", "cucumber", "bell pepper",
    "onion", "lettuce", "zucchini", "eggplant", "potato", "corn",
    "green beans", "radish", "celery", "cauliflower", "kale",
    "sweet potato", "peas", "beetroot", "asparagus", "artichoke",
    "brussels sprouts", "cabbage", "turnip", "garlic", "pumpkin",
    "butternut squash", "leek", "mushroom", "fennel", "chili pepper",
    "arugula", "watercress", "collard greens", "bok choy", "okra",
    "rutabaga", "rhubarb", "endive", "broccolini", "chard",
    "snow peas", "kohlrabi", "daikon", "mustard greens", "sugar snap peas",
    "cactus", "yam", "artichoke", "salsify", "chayote", "lotus root",
    "jicama", "tatsoi", "wasabi", "fiddlehead ferns", "dandelion greens",
    "chicory", "nopales", "malanga", "plantain", "purslane", "radicchio",
    "sunchokes", "karela", "taro", "water chestnut", "edamame", "luffa",
    "arrowroot", "lotus stem", "jute", "tepary beans", "opuntia", "samphire",
    "kale", "amame", "manioc", "agave", "winged beans", "cassava", "jerusalem artichoke",
    "kohlrabi", "yuca", "radicchio", "wasabi", "daikon", "yam bean", "hearts of palm",
    "spaghetti squash", "bamboo shoots", "chinese broccoli", "okra", "water spinach", "chinese cabbage",
    "bitter melon", "choy sum", "chinese chives", "gai lan", "shungiku", "tong ho",
    "tatsoi", "purple yam", "ice plant", "japanese pumpkin", "japanese radish", "konjac",
    "tubers", "tinda", "lamb's lettuce", "japanese parsley", "komatsuna", "sorrel"
];

const dishes = [
    "Vegetable stir-fry",
    "Roasted vegetable medley",
    "Grilled vegetable skewers",
    "Vegetable curry",
    "Garden salad",
    "Ratatouille",
    "Vegetable soup",
    "Stuffed bell peppers",
    "Veggie-packed pasta primavera",
    "Sauteed vegetables with garlic and herbs",
    "Vegetarian sushi rolls",
    "Vegetable frittata",
    "Veggie tacos or fajitas",
    "Caprese salad with tomatoes and basil",
    "Vegetable kebabs with a yogurt dip",
    "Greek salad with cucumbers, tomatoes, and feta cheese",
    "Vegetable quiche",
    "Buddha bowls with assorted roasted vegetables"
]

const injectData = async () => {
    await createDatabase()
    await Promise.all([Ingredient.sync()])

    const data = []

    for (let i = 0; i < 100_000; i++) {
        data.push({
            description: `${faker.helpers.arrayElement(dishes)} and ${faker.helpers.arrayElement(vegetables)}.`,
            kcal: faker.number.int({min: 4, max: 20}),
            protein: faker.number.int({min: 4, max: 20}),
            fat: faker.number.int({min: 4, max: 20}),
            carbs: faker.number.int({min: 4, max: 20}),
        })
    }

    await getModels().Ingredient.bulkCreate(data)
}

injectData().then(() => console.log("Data inserted!"))