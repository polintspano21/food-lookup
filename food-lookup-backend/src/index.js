import express from 'express'
import cors from 'cors'
import { createDatabase } from './db.js'
import Ingredient from './models/Ingredient.js'
import { router } from './controlers/ingredientControler.js'
const app = express()
const port = process.env.PORT || 8080;

const corsOptions = {
    origin: 'http://localhost:3000',
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(router)

app.listen(port, async () => {
    console.log(`This server work on ${port}`);
    await createDatabase()
    await Promise.all([Ingredient.sync()])
});
