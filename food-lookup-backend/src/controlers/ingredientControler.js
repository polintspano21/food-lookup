import express from 'express'
import { getModels } from '../db.js'
import { Op } from 'sequelize'

export const router = express.Router()

// offset -> 0
// limit -> 10
// offset -> 10
// limit -> 10

// Query params
// - description -> string to search by
// - offset -> from where to start
// - limit -> how many items per page
// - example url -> 'http://localhost:5000/ingredients?description="Potato"&offset="0"&limit="10"'
router.get('/ingredients', async (req, res) => {
    let description = req.query?.description ?? ''
    let offset = req.query?.offset ?? 0
    let limit = req.query?.limit ?? 10

    const response = await getModels().Ingredient.findAndCountAll({
        where: {
            description: {
                [Op.iLike]: `%${description.toLowerCase()}%`,
            },
        },
        offset: Number(offset),
        limit: Number(limit),
    })

    res.status(200).json({
        total: response.count,
        page: response.rows
    })
})

router.get('/ingredient/:id', async (req, res) => {
    const response = await getModels().Ingredient.findByPk(req.params.id)

    res.status(200).json(response)
})


router.post('/add', async (req, res) => {
    const response = await getModels().Ingredient.create(req.body)

    res.status(200).json(response)
})

router.put('/update/:id', async (req, res) => {
    const response = await getModels().Ingredient.update(req.body, { where: {id: req.params.id}, returning: true })

    res.status(200).json(response[1][0])
})

router.delete('/delete/:id', async (req, res) => {
    await getModels().Ingredient.destroy({ where: {id: req.params.id}, returning: true })

    res.status(200).json()
})