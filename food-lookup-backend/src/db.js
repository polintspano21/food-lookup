import { Sequelize } from 'sequelize'
import pg from 'pg';

const DB_USER = process.env.PGUSER;
const DB_HOST = process.env.PGHOST;
const DB_NAME = process.env.DATABASE;
const DB_PASSWORD = process.env.PGPASSWORD;

export const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
    host: DB_HOST,
    dialect: 'postgres',
});

export const createDatabase = async () => {
    const client = new pg.Client();

    try {
        await client.connect();
        const dbQuery = await client.query(`SELECT FROM pg_database WHERE datname = $1`, [DB_NAME]);

        if (dbQuery.rows.length === 0) await client.query(`CREATE DATABASE ${DB_NAME}`);
    } catch (error) {
        console.error(error);
    } finally {
        client.end();
    }
};

export const getModels = () => sequelize.models