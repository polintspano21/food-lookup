import { DataTypes } from 'sequelize'
import { sequelize } from '../db.js'

const Ingredient = sequelize.define('Ingredient', {
    description: { type: DataTypes.TEXT, allowNull: false },
    kcal: { type: DataTypes.FLOAT, allowNull: false },
    protein: { type: DataTypes.FLOAT, allowNull: false },
    fat: { type: DataTypes.FLOAT, allowNull: false },
    carbs: { type: DataTypes.FLOAT, allowNull: false },
}, {
    tableName: 'ingredient'
})

export default Ingredient